# SynchroNET BBS Dockerfile

FROM debian:buster-slim

MAINTAINER DuckHuntPr0
LABEL maintainer="DuckHuntPr0" version="-dev" description="Synchronet BBS Docker"


## Required
# http://wiki.synchro.net/install:nix:prerequisites
ENV REQUIRED_PACKS g++ make wget git nano less bash perl linux-libc-dev libgcrypt-dev libarchive-dev libcap-dev libncursesw5-dev libnspr4-dev python2 patch zip unzip pkgconf supervisor supervisor-doc tmux

#useful archive/compression tools
ENV UTILS_ARCHIVE lzip bzip2 xz-utils gzip tar rar unace p7zip archivemount advancecomp


RUN	echo "\033[1;32m########################################\n# Preparing base system..\n########################################\033[0m"  \
	&& echo "\n\033[1;93m* NOTE: Adding 'contrib' and 'non-free' repos to '/etc/apt/sources.list'\033[0m\n" \
	&& sed -i "s/deb.debian.org\/debian buster main/deb.debian.org\/debian buster main contrib non-free/g" /etc/apt/sources.list \
	&& apt-get -y update \
	&& apt-get -y upgrade \
	&& apt-get -y install ${REQUIRED_PACKS} ${UTILS_ARCHIVE}


RUN	echo "\033[1;32m########################################\n# Creating user 'bbs' and group 'bbs'..\n########################################\033[0m"  \
	&& useradd -m -s /bin/bash bbs


RUN	echo "\033[1;32m########################################\n# Downloading and building Synchronet..\n# (FROM: https://gitlab.synchro.net/main/sbbs/-/raw/master/install/GNUmakefile )\n########################################\033[0m" \
	&& su bbs -c "mkdir -p /home/bbs/sbbs" \
	&& su bbs -c "cd /home/bbs/sbbs && \
			wget https://gitlab.synchro.net/main/sbbs/-/raw/master/install/GNUmakefile && \
			make install SYMLINK=1 NOCAP=1 NO_X=1 NO_GTK=1 SBBSUSER=bbs SBBSGROUP=bbs"


RUN	echo "\033[1;32m########################################\n# Packing down the Synchronet build to 'sbbs_build.tar.gz' ..\n# (this will later be restored upon first boot..)\n########################################\033[0m" \
	&& sleep 8 \
	&& su bbs -c "tar -czvf /home/bbs/sbbs_build.tar.gz /home/bbs/sbbs" \
	&& su bbs -c "touch /home/bbs/.is_fresh_build" \
	&& su bbs -c "echo '1' > /home/bbs/.is_fresh_build"


RUN	echo "\033[1;32m########################################\n# Creating boot scripts..\n########################################\033[0m" \
	&& su bbs -c "cd ~ \
			&& touch .restore.sh \
			&& echo '#!/usr/bin/env bash' >> .restore.sh \
			&& echo 'cd /' >> .restore.sh \
			&& echo 'tar -xzvf /home/bbs/sbbs_build.tar.gz' >> .restore.sh \
			&& chmod +x .restore.sh \
			"

RUN	echo "\033[1;32m########################################\n# Cleaning up..\n########################################\033[0m" \
	&& apt-get clean \
	&& apt-get autoremove \
	&& apt-get autoclean \
	&& su bbs -c "rm -rfv /home/bbs/sbbs/*"

#	&& su mysticbbs -c "mkdir -p /home/mysticbbs/.config/supervisor-inc"


#EXPOSE 23/tcp
#EXPOSE 22/tcp
#EXPOSE 2222/tcp
#EXPOSE 2222/udp

#ENTRYPOINT ["/.boot.sh"]

#snips below
#	&& if [ ${CRYPTLIB_BUILD_EXIT_CODE} != 0]; then exit 1 ;fi \
#
# 	&& su mysticbbs -c "touch /home/mysticbbs/.fresh"


